#!/bin/bash

# set -e

source $OPENSHIFT_CARTRIDGE_SDK_BASH

source ${OPENSHIFT_DAC_DIR}/bin/dac-scripts/util-functions.sh

source ${OPENSHIFT_DAC_DIR}/bin/dac-scripts/setting-java-opt-environment-variables.sh

source ${OPENSHIFT_DAC_DIR}/bin/dac-scripts/setting-VCAP_SERVICES.sh

