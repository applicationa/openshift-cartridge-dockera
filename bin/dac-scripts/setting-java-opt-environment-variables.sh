#!/bin/bash 

# this file is sourced by main-helper.sh which in turn is sourced by all life-cycle scripts

# its purpose is to set environment variables that are used as command line parameters
# when the application (java) is started 

export OPENSHIFT_DAC_JAVA_OPTS_SERVER_NET=" -Dserver.address=${OPENSHIFT_DAC_IP} -Dserver.port=${OPENSHIFT_DAC_HTTP_PORT}"
if [[ -n "${OPENSHIFT_DAC_JAVA_OPTS_SERVER_NET_OVERRIDE}"  ]]; then
   OPENSHIFT_DAC_JAVA_OPTS_SERVER_NET=${OPENSHIFT_DAC_JAVA_OPTS_SERVER_NET_OVERRIDE}
fi


export OPENSHIFT_DAC_JAVA_OPTS=" -server -Dlogging.path=${OPENSHIFT_DAC_LOG_DIR} -Dspring.profiles.active=cloud"
if [[ -n "${OPENSHIFT_DAC_JAVA_OPTS_OVERRIDE}"  ]]; then
   OPENSHIFT_DAC_JAVA_OPTS=${OPENSHIFT_DAC_JAVA_OPTS_OVERRIDE}
fi
