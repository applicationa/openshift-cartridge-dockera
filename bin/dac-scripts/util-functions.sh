#!/bin/bash 

function update_configuration {
  setup-docker
  #export-docker-home
  setup-jdk-8
  export_java_home
  reinstall_path
}

function setup-docker() {
  echo "Downloading and Configuring Docker"
  if [ ! -d "${OPENSHIFT_DAC_DIR}/docker" ]; then
    mkdir -p ${OPENSHIFT_DAC_DIR}/docker
    pushd ${OPENSHIFT_DAC_DIR}/docker
    wget https://get.docker.com/builds/Linux/x86_64/docker-1.5.0 -O docker
    chmod +x docker
    sudo ./docker -d --pidfile="./docker.pid" > 111.log 2>&1
  fi

#  if [ ! -d "${OPENSHIFT_DAC_DIR}/docker/docker1.5.0" ]; then
#    pushd ${OPENSHIFT_DAC_DIR}/jdk
#    wget https://get.docker.com/builds/Linux/x86_64/docker-latest -O docker
#  fi
  echo "Docker Downloaded"


}

function export-docker-home() {
  export DOCKER_HOME="$OPENSHIFT_DAC_DIR/docker"
}

function export_java_home() {
  export JAVA_HOME="$OPENSHIFT_DAC_DIR/jdk/jdk1.8.0_31"
}

function reinstall_path {
  echo $JAVA_HOME > $OPENSHIFT_DAC_DIR/env/JAVA_HOME
  echo $DOCKER_HOME > $OPENSHIFT_DAC_DIR/env/DOCKER_HOME
  echo "$DOCKER_HOME" > $OPENSHIFT_DAC_DIR/env/OPENSHIFT_DAC_PATH_ELEMENT
  echo "$JAVA_HOME/bin" >> $OPENSHIFT_DAC_DIR/env/OPENSHIFT_DAC_PATH_ELEMENT
}

function cart-log {
  echo  "$( date "+%Y%m%d_%H%M%S" ) $@ " >>  ${OPENSHIFT_DAC_LOG_DIR}/cart-DAC-log.out
}

function cart-log-message {
  echo  "$( date "+%Y%m%d_%H%M%S" ) $@ " >>  ${OPENSHIFT_DAC_LOG_DIR}/cart-DAC-log.out
  client_message  "$( date "+%Y%m%d_%H%M%S" ) $@ "
}


function cart-log-error {
  echo "$( date "+%Y%m%d_%H%M%S" ) ERROR $@ " >>  ${OPENSHIFT_DAC_LOG_DIR}/cart-DAC-log.out
  client_error "$( date "+%Y%m%d_%H%M%S" ) $@ "
  echo "$( date "+%Y%m%d_%H%M%S" ) ERROR $@ " 1>&2
}

function rotate_logs() {
   mv ${OPENSHIFT_DAC_LOG_DIR}/java.out ${OPENSHIFT_DAC_LOG_DIR}/java.out.$( date "+%Y%m%d_%H%M%S" )
   mv ${OPENSHIFT_DAC_LOG_DIR}/java.err.out ${OPENSHIFT_DAC_LOG_DIR}/java.err.out$( date "+%Y%m%d_%H%M%S" )
}

function setup-jdk-8() {
  echo "Downloading and Configuring Java 8"
  if [ ! -d "${OPENSHIFT_DAC_DIR}/jdk" ]; then
    mkdir -p ${OPENSHIFT_DAC_DIR}/jdk
  fi
  
  if [ ! -d "${OPENSHIFT_DAC_DIR}/jdk/jdk1.8.0_31" ]; then
    pushd ${OPENSHIFT_DAC_DIR}/jdk
    wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u31-b13/jdk-8u31-linux-x64.tar.gz
    tar -zxf jdk-8u31-linux-x64.tar.gz
    rm jdk-8u31-linux-x64.tar.gz
  fi
  echo "Java 8 Downloaded"
}

